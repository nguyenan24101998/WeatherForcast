# WeatherForcast
An application which provides weather information

Build for studying purpose only

## Require

Min Sdk version 33+ device

Compile Sdk version 33

Use newest Android Studio's version or others IDE for running whole project

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
